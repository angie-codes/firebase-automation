require('dotenv').config()
const fs = require('fs')
const rp = require('request-promise')
const prompt = require('prompts')
const dayjs = require('dayjs')
const readline = require('readline')
const papa = require('papaparse')

/*
  1. init
  2. select file / enter path (last option)
  3. compiling to filename-done.csv

  // https://firebase.google.com/docs/reference/dynamic-links/link-shortener
*/

const nonCustomUrl = process.env.NON_CUSTOM_URL.split(',')

const listCsv = folder => {
  const file = fs.readdirSync(__dirname + folder, 'utf8')
  const filesToSort = []

  file.map(f => {
    if (f.includes('.csv')) {
      const info = fs.statSync(__dirname + folder + '/' + f)

      filesToSort.push({
        filename: f,
        time: dayjs(info.mtimeMs).format('YYYY-MM-DD HH:mm:ss')
      })
    }
  })

  const sorted = filesToSort.sort((a, b) => {
    if (dayjs(a.time).isBefore(dayjs(b.time))) {
      return 1
    } else {
      return -1
    }
  })

  const result = sorted.map(f => {
    return {
      tile: f.filename,
      value: f.filename
    }
  })

  result.unshift({
    title: 'Enter custom file path ✏️  (or select one from below)',
    value: 'customPath'
  })

  // console.log(result)
  return result
}

// triggered on EACH question submission
const onCancel = promptObj => {
  console.log('Bye 👋')
  return false
}

// triggered on EACH question submission
// have access to the submitted question obj, and the user's reply
const onSubmit = (promptObj, ans, anws) => {
  // return true to stop prompting
  // console.log(p, '---', ans, anws)
  // if (ans === 'b2-test.csv') {
  //   return {
  //     type: 'text',
  //     name: 'extra',
  //     message: 'hello!'
  //   }
  // }
}

const fileExists = async (path, flip) => {
  let result

  try {
    const customDir = path.split('/')
    const filename = customDir.pop()

    let str = customDir.join('/')
    const files = fs.readdirSync(str, 'utf8')

    if (files.includes(filename)) {
      if (flip) {
        result = 'File exists. (Backspace / Delete)'
      } else {
        result = true
      }
    } else {
      if (flip) {
        result = true
      } else {
        result = 'Couldn\'t find file.'
      }
    }
  } catch(err) {
    result = err.toString()
  }

  return result
}

const addDeepLink = link => {
  const deepLink = link.trim().split('?')[0].split(process.env.BASE_URL)[1] //domain.com/page?some=utm-here
  let newLink = link.trim()

  if (deepLink && deepLink.length) {
    if (nonCustomUrl.includes(deepLink.split('/')[0])) {
      newLink = link.trim() + '&deeplink=%2F' + deepLink.replace(/\//g, '%2F')
    } else {
      newLink = link.trim() + '&deeplink=%2Fcontent%2F' + deepLink.replace(/\//g, '%2F')
    }
  } else if (deepLink === undefined) {
    // will end program with error if link doesn't start with BASE_URL
    throw new Error('\x1b[31mPlease make sure your long link starts with: "' + process.env.BASE_URL + '"\x1b[0m')
  }

  return newLink
}


;(async () => {
  try {
    const user = await prompt([
      {
        type: 'select',
        name: 'src',
        message: '\x1b[0m\x1b[33mChoose your csv file\x1b[0m\n',
        choices: listCsv('/csv/links')
      },
      {
        type: prev => prev === 'customPath' ? 'text' : null,
        name: 'src',
        message: '\x1b[0m\x1b[33mOkay enter custom file path (absolute):\n(You can try drag and drop the csv into the terminal 👀 )\x1b[0m\n',
        format: value => value.replace(value.split('/')[value.split('/').length - 1], '') + '%div%' + value.split('/')[value.split('/').length - 1],
        validate: value => fileExists(value)
      },
      {
        type: 'text',
        name: 'target',
        message: prev => `\x1b[0m\x1b[33mHit enter to save as ${ prev.includes('%div%') ? prev.split('%div%')[1].replace('.csv', '') : prev.replace('.csv', '') }-done.csv in ${ __dirname}/compiled/links/\nOr enter custom compiled csv filename:\x1b[0m\n`,
        initial: value => value.includes('%div%') ? value.split('%div%')[1].replace('.csv', '-done.csv') : value.replace('.csv', '-done.csv'),
        // validate: value => fileExists(__dirname + '/compiled/links/' + value, 1)
      },
      {
        type: 'select',
        name: 'diffLanding',
        message: '\x1b[0m\x1b[33mWill this have different landing page between web and app?\x1b[0m\n',
        choices: [
          {
            title: 'no',
            value: false
          },
          {
            title: 'yes',
            value: true
          }
        ],
        initial: false
      },
      {
        type: 'select',
        name: 'appDownload',
        message: '\x1b[0m\x1b[33mDrive app download for this batch? (Leads to App/Play store page if no app)\x1b[0m\n',
        choices: [
          {
            title: 'no',
            value: false
          },
          {
            title: 'yes',
            value: true
          }
        ],
        initial: false
      }
    ], { onSubmit, onCancel })

    // terminate without error code
    if (!user.src || !user.target) {
      console.log('\x1b[31mSource and target paths cannot be empty. Please check your input again.\x1b[0m')
      return
    }

    const findFile = await fileExists(__dirname + '/compiled/links/' + user.target, 1)

    if (typeof findFile === 'string' && findFile.includes('File exists')) {
      user.action = await prompt([
        {
          type: 'select',
          name: 'overWrite',
          message: 'File exists. Overwrite or rename?',
          choices: [
            {
              title: 'Overwrite',
              value: true
            },
            {
              title: 'Rename',
              value: false
            }
          ]
        },
        {
          type: prev => !prev ? 'text' : null,
          name: 'newTarget',
          message: 'Enter new filename',
          validate: value => fileExists(__dirname + '/compiled/links/' + value, 1)
        }
      ])
    }

    if (user.appDownload === 'custom') {
      console.log('Please make sure you have a column header "drive-app" in your csv file with 1 or 0 as values.')
    }

    user.src = user.src.replace('%div%', '')
    if (!user.src.includes('/')) {
      user.src = __dirname + '/csv/links/' + user.src
    }

    if (user.action && user.action.newTarget) {
      user.target = user.action.newTarget
    }

    user.headers = {}
    // console.log(user) // { src: '/home/user/bla/bla.csv' | filename.csv, target: filename-done.csv }

    let count = 0
    const rs = fs.createReadStream(user.src)

    const rl = readline.createInterface({
      input: rs,
      crlfDelay: Infinity
    })

    let fileHeaders = []

    for await (const line of rl) {
      const { data } = papa.parse(line)
      const [ fields ] = data

      if (count === 0) {

        // checking header row
        if (user.diffLanding === true && (fields.indexOf('fallback-ios') === -1 || fields.indexOf('fallback-android') === -1)) {
          throw new Error('\x1b[31mTo have different landing rules for web and app, please provide "fallback-ios" and "fallback-android" headers in your csv file.\x1b[0m')
        }

        fields.forEach(h => {
          user.headers[h] = ''
        })

        user.headers.fallback = ''
        user.headers.dynamic = ''

        // terminate program if couldn't find link column
        if (fields.indexOf('link') === -1) {
          throw new Error('"link" column not found. Please check your csv file.')
        }

        // will create a fresh file with empty content on program re-run
        fs.writeFileSync(__dirname + '/compiled/links/' + user.target, '', 'utf8')
        fs.writeFileSync(
          __dirname + '/compiled/links/' + user.target,
          Object.keys(user.headers).join(',') + '\n',
          'utf8'
        )

        fileHeaders = Object.keys(user.headers)
        // move utms

      } else {
        const fallbackLinks = device => {
          let result = ''

          if (user.appDownload === false) {
            if (user.diffLanding === true) {
              if (device === 'android') {
                result = addDeepLink(fields[Object.keys(user.headers).indexOf('fallback-android')])
              } else if (device === 'ios') {
                result = addDeepLink(fields[Object.keys(user.headers).indexOf('fallback-ios')])
              }
            } else {
              result = addDeepLink(fields[Object.keys(user.headers).indexOf('link')])
            }
          }

          return result
        }

        const generateLink = async () => {
          const result = await rp({
            uri: process.env.REQUEST_URI + process.env.APIKEY,
            method: 'POST',
            body: JSON.stringify({
              dynamicLinkInfo: {
                domainUriPrefix: process.env.URI_PREFIX,
                link: addDeepLink(fields[Object.keys(user.headers).indexOf('link')]),
                androidInfo: {
                  androidPackageName: process.env.APACKAGENAME,
                  androidFallbackLink: fallbackLinks('android'),
                },
                iosInfo: {
                  iosBundleId: process.env.IPACKAGENAME,
                  iosFallbackLink: fallbackLinks('ios'),
                  iosAppStoreId: user.appDownload ? process.env.APPSTOREID : ''
                }
              },
              suffix: {
                option: 'SHORT'
              }
            }),
            simple: false
          })

          const { shortLink } = JSON.parse(result)
          return shortLink
        }

        for (let j = 0; j < fileHeaders.length; j++) {
          user.headers[fileHeaders[j]] = fields[j]
        }

        user.headers.fallback = addDeepLink(user.headers.link)

        try {
          user.headers.dynamic = await generateLink()

          fs.appendFileSync(
            __dirname + '/compiled/links/' + user.target,
            Object.values(user.headers).join(',') + '\n',
            'utf8'
          )
        } catch (error) {
          console.log('\x1b[31mError generating dynamic link:\x1b[0m', error)
        }
      }

      // clear header values after inserting
      fileHeaders.forEach(el => {
        user.headers[el] = ''
      })

      count++
    }
  } catch (e) {
    console.log('\x1b[31m%s\x1b[0m', 'Opps, code exited with below error:')
    console.log(e)
  }
})()
