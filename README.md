# Firebase Dynamic Links - Bulk Generation

Step 1: Clone & Install

```bash
git clone git@gitlab.com:angie-codes/firebase-automation.git
cd firebase-automation
yarn install
```

Step 2: Create a `.env` file in project root with data all filled in (as per sample given in `.env.sample`)

Step 3: Init program
```bash
yarn csv
```

Step 4: Follow thru terminal UI and wait for the program to complete.

---

**✨ To receive updates, simply run:**
```bash
cd path/to/firebase-automation
git pull
```
