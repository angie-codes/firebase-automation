require('dotenv').config()
const fs = require('fs')
const axios = require('axios')
const prompt = require('prompts')
const dayjs = require('dayjs')
const readline = require('readline')
const papa = require('papaparse')
const { getAccessTkn } = require('./token')

/*
  1. yarn stats
  2. select file / enter path (last option)
  3. fetch for stats

  https://firebase.google.com/docs/reference/dynamic-links/analytics#node.js
*/

const listCsv = folder => {
  const file = fs.readdirSync(__dirname + folder, 'utf8')
  const filesToSort = []

  file.map(f => {
    if (f.includes('.csv')) {
      const info = fs.statSync(__dirname + folder + '/' + f)

      filesToSort.push({
        filename: f,
        time: dayjs(info.mtimeMs).format('YYYY-MM-DD HH:mm:ss')
      })
    }
  })

  const sorted = filesToSort.sort((a, b) => {
    if (dayjs(a.time).isBefore(dayjs(b.time))) {
      return 1
    } else {
      return -1
    }
  })

  const result = sorted.map(f => {
    return {
      tile: f.filename,
      value: f.filename
    }
  })

  result.unshift({
    title: 'Enter custom file path ✏️  (or select one from below)',
    value: 'customPath'
  })

  // console.log(result)
  return result
}

const fileExists = async (path, flip) => {
  let result

  try {
    const customDir = path.split('/')
    const filename = customDir.pop()

    let str = customDir.join('/')
    const files = fs.readdirSync(str, 'utf8')

    if (files.includes(filename)) {
      if (flip) {
        result = 'File exists. (Backspace / Delete)'
      } else {
        result = true
      }
    } else {
      if (flip) {
        result = true
      } else {
        result = 'Couldn\'t find file.'
      }
    }
  } catch(err) {
    result = err.toString()
  }

  return result
}

const massagePathData = input => {
  // this function inserts a mark up ('%div%') between file path & filename for easy splitting later
  // from /absolute/path/to/filename.ext
  // to /absolute/path/to/%div%filename.ext

  const filename = input.split('/')[input.split('/').length - 1] // filename.ext
  const pathToFile = input.replace(filename, '')
  return pathToFile + '%div%' + filename
}


;(async () => {
  try {
    const token = await getAccessTkn()
    // console.log(token)

    async function getLinkData (dynolink, duration) {
      const response = await axios.get(
        process.env.REQUEST_URI_STATS +
        encodeURIComponent(dynolink) +
        process.env.URI_POSTFIX + duration,
        {
          headers: {
            Authorization: 'Bearer ' + token
          }
        }
      )

      const results = response.data.linkEventStats
      // console.log(results.length)
      // console.log(results)
      return results
    }

    const user = await prompt([
      {
        type: 'select',
        name: 'src',
        message: '\x1b[0m\x1b[33mChoose your csv file\x1b[0m\n',
        choices: listCsv('/csv/stats')
      },
      {
        type: prev => prev === 'customPath' ? 'text' : null,
        name: 'src',
        message: '\x1b[0m\x1b[33mOkay enter custom file path (absolute):\n(You can try drag and drop the csv into the terminal 👀 )\x1b[0m\n',
        format: value => massagePathData(value),
        validate: value => fileExists(value)
      },
      {
        type: 'text',
        name: 'target',
        message: prev => `\x1b[0m\x1b[33mHit enter to save as ${ prev.includes('%div%') ? prev.split('%div%')[1].replace('.csv', '') : prev.replace('.csv', '') }-done.csv in ${ __dirname}/compiled/stats/\nOr enter custom compiled csv filename:\x1b[0m\n`,
        initial: value => value.includes('%div%') ? value.split('%div%')[1].replace('.csv', '-done.csv') : value.replace('.csv', '-done.csv'),
      },
      {
        type: 'number',
        name: 'days',
        message: 'Number of days to backdate:\n',
        validate: daysInput => daysInput < 1 ? 'Positive numbers only' : true
      },
    ])

    // console.log(user) // { src: 'x.csv', target: 'x-done.csv', days: 90 }

    // terminate without error code
    if (!user.src || !user.target) {
      console.log('\x1b[31mSource and target paths cannot be empty. Please check your input again.\x1b[0m')
      return
    }

    // check if output file exists
    const findFile = await fileExists(__dirname + '/compiled/stats/' + user.target, 1)

    // if exists, prompt user to override/rename
    if (typeof findFile === 'string' && findFile.includes('File exists')) {
      user.action = await prompt([
        {
          type: 'select',
          name: 'overWrite',
          message: 'File exists. Overwrite or rename?',
          choices: [
            {
              title: 'Overwrite',
              value: true
            },
            {
              title: 'Rename',
              value: false
            }
          ]
        },
        {
          type: prev => !prev ? 'text' : null,
          name: 'newTarget',
          message: 'Enter new filename',
          validate: value => fileExists(__dirname + '/compiled/stats/' + value, 1)
        }
      ])
    }

    // massage path data
    user.src = user.src.replace('%div%', '')
    if (user.src.includes('/') === false) {
      user.src = __dirname + '/csv/stats/' + user.src
    }

    // create empty file
    fs.writeFileSync(__dirname + '/compiled/stats/' + user.target, '', 'utf8')

    let count = 0
    const rs = fs.createReadStream(user.src)

    const rl = readline.createInterface({
      input: rs,
      crlfDelay: Infinity
    })

    let userHeader = ''
    let columnIndex
    const today = dayjs()
    const startDate = today.subtract(user.days, 'd').format('YYYY-MM-DD')

    console.log('\nFetching stats...')

    for await (const line of rl) {
      const { data } = papa.parse(line)
      const [ fields ] = data

      // skip header row
      if (count === 0) {
        userHeader = fields.join(',')
        columnIndex = fields.indexOf('dynamic')

        // if column not found, throw error
        if (columnIndex === -1) {
          throw new Error('\x1b[31mColumn "dynamic" not found. Please check your csv file again.\x1b[0m')
        }
      } else {
        const stats = await getLinkData(fields[columnIndex], user.days)

        const resultsRow = {
          'start-date': startDate,
          'days-from-today': user.days,
          'CLICK-ios': 0,
          'CLICK-android': 0,
          'CLICK-other': 0,
          'REDIRECT-ios': 0,
          'REDIRECT-android': 0,
          'REDIRECT-other': 0,
          'APP_RE_OPEN-ios': 0,
          'APP_RE_OPEN-android': 0,
          'APP_FIRST_OPEN-ios': 0,
          'APP_FIRST_OPEN-android': 0,
          'APP_INSTALL-android': 0,
        }

        // apparently some results can be empty
        if (stats) {
          for (let i = 0; i < stats.length; i++) {
            const statsObj = stats[i]
            resultsRow[statsObj.event + '-' + statsObj.platform.toLowerCase()] = statsObj.count
          }
        }

        // add insert header
        if (count === 1) {
          fs.writeFileSync(
            __dirname + '/compiled/stats/' + user.target,
            userHeader + ',' + Object.keys(resultsRow).join(',') + '\n',
            'utf8'
          )
        }

        fs.appendFileSync(
          __dirname + '/compiled/stats/' + user.target,
          fields.join(',') + ',' + Object.values(resultsRow).join(',') + '\n',
          'utf8'
        )
      }

      count++
    }
  } catch (error) {
    // only gives error code doesn't help
    // console.log(error.toString())
    console.log(error)
  }
})()
